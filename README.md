# Encrypt text as markov chains

The key provider takes a pair of file, source to read as text.
All words are collected and prepared for being used as markov chain.

Then encoder will then pick words from the probability lists where 
a high bit is encoded as odd index in the list, and a low bit as even 
index.

Elements that have zero links will trigger a new Chain to start and 
elements with one link will be skipped as no information can be
encoded into that.

The result is one of the usual markov chain gibberish texts, that 
remotely resemble the original text make make no sense whatsoever.

This libabry offers en-/decoders for bytewise encryption/decryption
or blobs not bigger than 5 kiB and and stream wrappers for continuous 
en-/decryption on the fly.

# Motivation
None, really. It was just another regular sunday.

# Example
```Java
private static File cache = new File("key2.txt");
private static final String keySrc = "https://....txt";
private static KeyProvider key;
private static MarkovChainEncoder encoder;
private static MarkovChainDecoder decoder;

public static void main(String[] args) {
	key = new KeyProvider();
	encoder = new MarkovChainEncoder(key);
	decoder = new MarkovChainDecoder(key);
	key.parseKey(cache, keySrc);

	StringBuilder enc = new StringBuilder();
	String message = "Hello World";
	encoder.appendValue(enc, message);
	message = enc.toString();
	System.out.printf("Ecoded: %s\n", message);
	enc.reverse();
	ByteBuffer buffer = ByteBuffer.wrap(new byte[1024*5]);
	try {
		decoder.collectBits(message);
		while (decoder.getBuffer(buffer)>0) ;//read all
		message = new String(buffer.array(), StandardCharsets.UTF_8);
		System.out.printf("Decoded: %s\n", message);
	} catch (IOException|DecoderException e) {
		System.err.println(e.getMessage());
	}

	try {
		MarkovChainStreamEncoder streamEncoder = new MarkovChainStreamEncoder(key, System.out);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		for(;;) {
			String line = br.readLine();
			streamEncoder.write(line.getBytes(StandardCharsets.UTF_8));
		}
	} catch (IOException ex) {
		ex.printStackTrace();
	}

}
```