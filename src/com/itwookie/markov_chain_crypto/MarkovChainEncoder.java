package com.itwookie.markov_chain_crypto;

import com.itwookie.markov_chain_crypto.keygen.KeyProvider;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.LinkedList;
import java.util.List;

public class MarkovChainEncoder {

    private SecureRandom rng = new SecureRandom();
    private KeyProvider key;
    public MarkovChainEncoder(KeyProvider key) {
        this.key = key;
    }

    /**
     * Appends one or more words to the buffer, until the chain offerd more than one choice.<br>
     * If the chain offered 0 choice a new random word will be appended in hoped to have more choice.<br>
     * On highValue a word from the latter half of the choice list will be chosen, otherwise from the
     * first half using
     * <pre>
     *
     * </pre>
     * Hitting a punctuation will reset the last used word to null, causing the chain to reset and
     * start with another keyList word. Same goes for chains that end with 0 choice.
     */
    private String lastUsedWord=null;
    private List<String> chain=new LinkedList<>();
    /** protection against endless loops for chains that never branch */
    private int noChainChain = 0;
    private void appendValue(StringBuilder buffer, boolean highValue) {
        while (chain.size()<2) {
            if (chain.size()==0) {
                appendSingleWord(buffer, key.getChainStarter(rng));
            } else {
                appendSingleWord(buffer, chain.get(0));
            }
            if (++noChainChain > 64) {
                throw new RuntimeException("The chain does not appear to be suitable for encoding");
            }
        }
        noChainChain = 0;

        //get next normalized gaussian random choice
        //prioritizes "frequent" words (index 0)
        double choice = rng.nextGaussian();
        if (choice < 0) choice =- choice; //positive half of gaussian curve
        //choose a base index using the gaussian choice, index is always even by int(x/2)*2
        int baseIndex = (int)Math.floor((chain.size() * choice)/2.0)*2;
        //encode bit information
        if (highValue)
            baseIndex++;
        //edge-case where index could fall outside valid range
        while (baseIndex>=chain.size())
            baseIndex-=2;
        //append encoded information
        appendSingleWord(buffer, chain.get(baseIndex));
    }

    /** internal, used by appendValue */
    private void appendSingleWord(StringBuilder buffer, String word) {
        lastUsedWord = word;
        buffer.append(word);
        buffer.append(' ');
        chain = lastUsedWord==null
                ? new LinkedList<>()
                : key.getChainFor(lastUsedWord);
    }

    /** Append all bits in this Byte LSB first using appendValue(StringBuffer, boolean) */
    public void appendValue(StringBuilder buffer, byte value) {
        for (int i=0; i<8; i++) {
            appendValue(buffer, (value & 0x01)>0);
            value = (byte) (value >>> 0x01);
        }
    }

    /** Append all bytes in this list using appendValue(StringBuffer, byte) */
    public void appendValue(StringBuilder buffer, byte[] bytes) {
        for (byte b : bytes) appendValue(buffer, b);
    }

    /** Append the string as String.getBytes(StandardCharsets.UTF_8) using appendValue(StringBuffer, byte[]) */
    public void appendValue(StringBuilder buffer, String message) {
        appendValue(buffer, message.getBytes(StandardCharsets.UTF_8));
    }

}
