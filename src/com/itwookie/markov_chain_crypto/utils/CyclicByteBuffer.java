package com.itwookie.markov_chain_crypto.utils;

import java.io.IOException;

/** this class is not designed for async operation! */
public class CyclicByteBuffer {

    private byte[] buffer;
    private int readPointer = 0;
    private int writePointer = 0;

    public CyclicByteBuffer(int capacity) {
        buffer = new byte[capacity];
    }

    public int buffered() {
        return writePointer >= readPointer
                ? writePointer-readPointer
                : (writePointer+buffer.length)-readPointer;
    }
    public int remaining() {
        return buffer.length-buffered();
    }

    public void pushByte(byte value) throws IOException {
        if (remaining()<=0)
            throw new IOException("Buffer can't accept any more data");
        buffer[writePointer++]=value;
        if (writePointer>=buffer.length)
            writePointer=0;
    }
    /** @return the byte value or -1 if buffered() == 0 */
    public int popByte() {
        if (buffered()<=0) return -1;
        byte v = buffer[readPointer++];
        if (readPointer>=buffer.length)
            readPointer=0;
        return ((int)v)&0x0ff;
    }

    /** @return the amount of bytes written */
    public int pushBytes(byte[] bytes) throws IOException {
        int cap = Math.min(remaining(), bytes.length);
        for (int i = 0; i < cap; i++) {
            pushByte(bytes[i]);
        }
        return cap;
    }
    /** @return the amount of bytes read from the buffer */
    public int popBytes(byte[] buffer) throws IOException {
        int cap = Math.min(buffered(), buffer.length);
        for (int i = 0; i < cap; i++) {
            int v = popByte();
            if (v<0)
                throw new IOException("Unexpected end of data");
            buffer[i] = (byte)v;
        }
        return cap;
    }

}
