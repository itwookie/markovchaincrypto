package com.itwookie.markov_chain_crypto.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    /**
     * //Pattern.compile("(?:^|\\b)[\\p{L}]+[^\\s]?(?:\\b|\\B|$)");
     * emulate word boundaries with lookahead/lookbehind to support unicode
     * (?<!\w)(?=\w) == \m
     * (?<=\w)(?!\w) == \M
     * also: a boundary check like this obsoletes start end end matching ^(.*)$
     */
    private static final Pattern word = Pattern.compile("(?<!\\p{L})(?=\\p{L})[\\p{L}]+[.,:;?!]?(?<=[\\p{L}.,:;?!?])(?![\\p{L}.,:;?!?])");

    public static List<String> filterWords(String line) {
        List<String> hits = new LinkedList<>();
        Matcher matcher = word.matcher(line);
        while (matcher.find()) {
            String match = matcher.group();
            hits.add(match);
        }
        return hits;
    }

}
