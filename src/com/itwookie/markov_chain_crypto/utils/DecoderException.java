package com.itwookie.markov_chain_crypto.utils;

/**
 * This exception occurs when the word stream hits an entry that's not in the
 * list of words for the chains provided through the KeyProvider.<br>
 * Usually this means the key was incorrect or the message was tinkered with.
 */
public class DecoderException extends Exception {
    /**
     * This exception occurs when the word stream hits an entry that's not in the
     * list of words for the chains provided through the KeyProvider.<br>
     * Usually this means the key was incorrect or the message was tinkered with.
     */
    public DecoderException(String encoded, String message) {
        super("Unable to decode \""+encoded+"\": "+message);
    }

}
