package com.itwookie.markov_chain_crypto.utils;

import java.util.*;

/** purpose of this class is to count how many times an item occurs within a collection
 * of items, eg words withing a text. the additional methods to retrieve as map allows
 * sorting the result by frequency or probability. */
public class CountingSet<T> implements Set<T> {

    private Map<T, Integer> values;
    private Long combinedCount=0L;
    public CountingSet() {
        values = new HashMap<>();
    }

    @Override
    public int size() {
        return values.size();
    }

    @Override
    public boolean isEmpty() {
        return values.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return values.containsKey(o);
    }

    @Override
    public Iterator<T> iterator() {
        return values.keySet().iterator();
    }

    @Override
    public Object[] toArray() {
        return values.keySet().toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return values.keySet().toArray(a);
    }

    @Override
    public boolean add(T t) {
        boolean res = values.containsKey(t);
        combinedCount++;
        values.put(t, values.getOrDefault(t, 0));
        return res;
    }

    @Override
    public boolean remove(Object o) {
        int v = values.getOrDefault(o, 0);
        if (v == 0) {
            return false;
        } else if (v == 1) {
            combinedCount--;
            values.remove(o);
            return true;
        } else {
            combinedCount--;
            values.put((T)o, v-1);
            return false;
        }
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return values.keySet().containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean changed = false;
        for (T e : c) changed |= add(e);
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean changed = false;
        Set<T> keys = new HashSet<>(values.keySet());
        for (T k : keys) if (!c.contains(k)) changed |= remove(k);
        return changed;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean changed = false;
        for (Object e : c) changed |= remove(e);
        return changed;
    }

    @Override
    public void clear() {
        values.clear();
    }

    public Map<T, Integer> getAsMap() {
        return new HashMap<>(values);
    }

    /** combined value of all entry counters */
    public Long getCombinedCount() {
        return combinedCount;
    }

    /** counters are normalized to combined count */
    public Map<T, Double> getWeightMap() {
        Map<T, Double> wm = new HashMap<>();
        for (Map.Entry<T, Integer> e : values.entrySet()) {
            wm.put(e.getKey(), (double)e.getValue()/combinedCount);
        }
        return wm;
    }
}
