package com.itwookie.markov_chain_crypto.keygen;

import com.itwookie.markov_chain_crypto.utils.CountingSet;
import com.itwookie.markov_chain_crypto.utils.StringUtils;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.function.ToDoubleFunction;

public class KeyProvider {

    private List<String> keyList = new LinkedList<>(); //list of keys that can initiate a new sentence, that is starting wuth \p{Lu}
    private Map<String, List<String>> chains = new HashMap<>();

    public void parseKey(File cache, @Nullable String resource) {
        assert cache != null : "No cache specified";
        if (!cache.exists()) {
            assert resource != null : "Can't fetch resource from NULL";
            cache.getAbsoluteFile().getParentFile().mkdirs();

            InputStream in=null;
            OutputStream out=null;
            byte[] buffer = new byte[1024*5]; //5kB buffer
            int r;
            try {
                HttpURLConnection con = (HttpURLConnection) (new URL(resource).openConnection());
                con.setDoInput(true);
                assert con.getResponseCode() == 200 : "Connection refused";

                out = new FileOutputStream(cache);
                in = con.getInputStream();

                while ((r=in.read(buffer))>=0) {
                    out.write(buffer, 0, r);
                }

                out.flush();

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try { in.close(); } catch (Exception ignore) {}
                try { out.close(); } catch (Exception ignore) {}
            }
        }

        Map<String, CountingSet<String>> associations = new HashMap<>();
        BufferedReader in = null;
        String previousWord = null;
        String spaceTemplate = new String(new char[80]).replace('\0', ' ');
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(cache)));
            String line;
            while ((line = in.readLine())!=null) {

                for (String match : StringUtils.filterWords(line)) {
                    if (previousWord != null) {
                        CountingSet<String> s = associations.getOrDefault(previousWord, new CountingSet<>());
                        s.add(match);
                        associations.put(previousWord, s);
                    }
                    previousWord = match;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try { in.close(); } catch (Exception ignore) {}
        }

        chains.clear();
        keyList.clear();
        for (Map.Entry<String, CountingSet<String>> e : associations.entrySet()) {
            List<String> sorted = sortByValue(e.getValue().getWeightMap());
            chains.put(e.getKey(), sorted);
            if (Character.isUpperCase(e.getKey().charAt(0)))
                keyList.add(e.getKey());
        }
    }

    private static List<String> sortByValue(Map<String, Double> map) {
        List<Map.Entry<String, Double>> list = new ArrayList<>(map.entrySet());
        list.sort(
                Comparator
                        .comparingDouble((ToDoubleFunction<Map.Entry<String, Double>>) Map.Entry::getValue)
                        .thenComparing(Map.Entry::getKey)
        );

        List<String> result = new LinkedList<>();
        for (Map.Entry<String, Double> entry : list) {
            result.add(entry.getKey());
        }

        return result;
    }

    public List<String> getChainFor(String previousWord) throws NoSuchElementException {
        List<String> result = chains.get(previousWord);
        if (result==null)
            throw new NoSuchElementException("\""+previousWord+"\" has no chain");
        return result;
    }
    public String getChainStarter(Random rng) {
        return keyList.get(rng.nextInt(keyList.size()));
    }

}
