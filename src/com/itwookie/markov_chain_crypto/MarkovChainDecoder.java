package com.itwookie.markov_chain_crypto;

import com.itwookie.markov_chain_crypto.keygen.KeyProvider;
import com.itwookie.markov_chain_crypto.utils.CyclicByteBuffer;
import com.itwookie.markov_chain_crypto.utils.DecoderException;
import com.itwookie.markov_chain_crypto.utils.StringUtils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.NoSuchElementException;

public class MarkovChainDecoder {

    private KeyProvider key;
    public MarkovChainDecoder(KeyProvider key) {
        this.key = key;
    }

    private String previousWord = null;
    private int accu = 0b00000000;
    private int accuCount = 0;
    private CyclicByteBuffer buffer = new CyclicByteBuffer(1024*5);

    /**
     * Can collect 5 kB of information at once, resulting in roughly
     * 40_000 words at a time, before the internal buffer overflows
     */
    public void collectBits(String text) throws DecoderException {
        for (String word : StringUtils.filterWords(text)) {
            if (previousWord!=null) {
                List<String> chain;
                try {
                    chain = key.getChainFor(previousWord);
                } catch (NoSuchElementException e) {
                    throw new DecoderException(previousWord, "The chain does not contain this word");
                }
                if (chain.size()>1) {
                    //if the chain for this word has less than 2 options, no information is encoded
                    int pos = chain.indexOf(word);
                    if (pos < 0)
                        throw new DecoderException(previousWord + " " + word, "The chain can't connect these words");

                    boolean value = ((pos % 2) == 1);

                    accu = accu >>> 1;
                    accu |= value ? 0x080 : 0x000;
                    if (++accuCount >= 8) {
                        try {
                            buffer.pushByte((byte) accu);
                        } catch (IOException e) {
                            throw new DecoderException(word, e.getMessage());
                        }
                        accu = 0;
                        accuCount = 0;
                    }
                }
            }
            previousWord = word;
        }
    }

    /** collect the complete internal buffer into a new byte buffer.
     * if nothing was buffered internally, null is returned */
    public ByteBuffer getBuffer() throws IOException {
        int amount = buffer.buffered();
        if (amount == 0)
            return null;
        byte[] back = new byte[amount];
        buffer.popBytes(back);
        return ByteBuffer.wrap(back);
    }
    /** collect as much data as possible into an existing buffer.<br>
     * The byte buffy given to this method is not reversed by this function;
     * this allows for this function to be called repeatedly until this function
     * returns 0, indicating the given buffer is either full or the internal
     * buffer is now empty. */
    public int getBuffer(ByteBuffer bytebuffer) throws IOException {
        int amount = Math.min(buffer.buffered(), bytebuffer.remaining());
        if (amount > 0) {
            byte[] tmp = new byte[amount];
            buffer.popBytes(tmp);
            bytebuffer.put(tmp);
        }
        return amount;
    }
    /** collect up to len bytes of data into an existing buffer.<br>
     * The byte buffy given to this method is not reversed by this function;
     * this allows for this function to be called repeatedly until this function
     * returns 0, indicating the given buffer is either full or the internal
     * buffer is now empty. */
    public int getBuffer(ByteBuffer bytebuffer, int len) throws IOException {
        assert len>=0 : "Can't read negative length";
        int amount = Math.min(buffer.buffered(), bytebuffer.remaining());
        amount = Math.min(amount, len);
        if (amount > 0) {
            byte[] tmp = new byte[amount];
            buffer.popBytes(tmp);
            bytebuffer.put(tmp);
        }
        return amount;
    }

    /** return a single byte from the buffer or -1 if none are available */
    public int get() {
        return buffer.popByte();
    }

    /** expose cycling buffer value */
    public int available() {
        return buffer.buffered();
    }

    /** expose cycling buffer value */
    public int remaining() {
        return buffer.remaining();
    }

    /** expose cycling buffer value */
    public int capacity() {
        return 1024*5;
    }

}
