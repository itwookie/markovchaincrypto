package com.itwookie.markov_chain_crypto;

import com.itwookie.markov_chain_crypto.utils.DecoderException;
import com.itwookie.markov_chain_crypto.keygen.KeyProvider;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MarkovChainStreamDecoder extends InputStream {

    private InputStreamReader wrapping;
    private MarkovChainDecoder decoder;
    private boolean reachedEOF = false;

    public MarkovChainStreamDecoder(KeyProvider key, InputStream wrapping) {
        this.wrapping = new InputStreamReader(wrapping);
        this.decoder = new MarkovChainDecoder(key);
    }

    /** @return the amount of bytes successfully parsed */
    private long readNWords(long n) throws IOException {
        if (reachedEOF)
            return 0;

        int bytesPre = decoder.available();
        StringBuilder sb = new StringBuilder();
        boolean inWord = false, wasBoundary=true;
        int wordsRead = 0;
        int c;
        while(wordsRead < n) {
            c=wrapping.read();
            if (c < 0) {
                reachedEOF = true;
                break;
            }
            char ch = (char)c;
            sb.append(ch);
            if (Character.isLetter(ch) && wasBoundary) {
                inWord = true;
                wasBoundary = false;
            } else if (Character.isSpaceChar(ch) && inWord) {
                inWord = false;
                wordsRead++;
                wasBoundary = true;
            } else { //for numbers and such
                wasBoundary = false;
            }
        }
        try {
            decoder.collectBits(sb.toString());
        } catch (DecoderException e) {
            throw new IOException("Could not decode bytes", e);
        }
        return decoder.available()-bytesPre;
    }
    /**
     * Tries to read a word count approximation for the amount of bytes until
     * no further words can be read or enough words were read for byte conversion
     * @return the amount of bytes read
     */
    private long readNBytes(long n) throws IOException {
        long bytesRead = 0;
        while (bytesRead < n) {
            long read = readNWords((n-bytesRead)*8); //approx 8 words per byte
            if (read < 1) break; //no more bytes to read from stream (or maaany <2 choice chains)
            bytesRead += read;
        }
        return bytesRead;
    }

    @Override
    public int read(@NotNull byte[] b, int off, int len) throws IOException {
        assert off+len < b.length : "Your specified buffer is not big enough to hold n (Arg3) bytes at offset (Arg2)";
        int amount = (int)readNBytes(len);
        if (amount < 1) return reachedEOF?-1:0;
        decoder.getBuffer().get(b,off,amount);
        return amount;
    }

    @Override
    public long skip(long n) throws IOException {
        long c = readNBytes(n);
        decoder.getBuffer();
        return c;
    }

    @Override
    public int available() {
        return reachedEOF?0:1;
    }

    @Override
    public void close() throws IOException {
        wrapping.close();
    }

    @Override
    public int read() throws IOException {
        if (reachedEOF)
            return -1;
        int n = (int)readNBytes(1);
        if (n == 0) {
            if (!reachedEOF)
                throw new IOException("Unexpected read error");
            else
                return -1;
        }
        return decoder.get();
    }
}
