package com.itwookie.markov_chain_crypto;

import com.itwookie.markov_chain_crypto.keygen.KeyProvider;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class MarkovChainStreamEncoder extends OutputStream {

    private OutputStream wrapping;
    private MarkovChainEncoder encoder;
    private StringBuilder buffer;

    public MarkovChainStreamEncoder(KeyProvider key, OutputStream wrapping) {
        this.wrapping = wrapping;
        this.encoder = new MarkovChainEncoder(key);
        this.buffer = new StringBuilder();
    }

    @Override
    public void write(@NotNull byte[] b, int off, int len) throws IOException {
        for (int i=off; i<off+len; i++) {
            encoder.appendValue(buffer, b[i]);
        }
        wrapping.write(buffer.toString().getBytes(StandardCharsets.UTF_8));
        buffer.setLength(0);
    }

    @Override
    public void flush() throws IOException {
        wrapping.flush();
    }

    @Override
    public void close() throws IOException {
        wrapping.close();
    }

    @Override
    public void write(int b) throws IOException {
        encoder.appendValue(buffer, (byte)(b&0x0ff));
        wrapping.write(buffer.toString().getBytes(StandardCharsets.UTF_8));
        buffer.setLength(0);
    }
}
